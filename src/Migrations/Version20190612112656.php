<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190612112656 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product CHANGE name name TINYTEXT NOT NULL, CHANGE price price LONGTEXT NOT NULL, CHANGE inr inr LONGTEXT NOT NULL, CHANGE eur eur LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE expense ADD inr VARCHAR(233) NOT NULL, ADD eur VARCHAR(233) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE expense DROP inr, DROP eur');
        $this->addSql('ALTER TABLE product CHANGE name name TINYTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE inr inr LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE eur eur LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE price price LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
