<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190612093636 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product ADD inr LONGTEXT NOT NULL, ADD eur LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE expense CHANGE uid uid VARCHAR(122) NOT NULL, CHANGE pid pid VARCHAR(233) NOT NULL, CHANGE rate rate VARCHAR(233) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE expense CHANGE uid uid VARCHAR(122) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE pid pid VARCHAR(233) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE rate rate VARCHAR(233) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE product DROP inr, DROP eur');
    }
}
