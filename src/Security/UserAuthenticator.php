<?php
// src/Security/LoginFormAuthenticator.php
namespace App\Security;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;


class UserAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;

    private $entityManager;
    private $router;
    private $csrfTokenManager;
    private $passwordEncoder;

    public function __construct(EntityManagerInterface $entityManager, RouterInterface $router, CsrfTokenManagerInterface $csrfTokenManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function supports(Request $request)
    {
    
        return 'login' === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    public function getCredentials(Request $request)
    {
        $credentials = [
            'username' => $request->request->get('_username'),
            'password' => $request->request->get('_password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['username']
        );
       
        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }
       
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $credentials['username']]);
        
        if (!$user) {
            // fail authentication with a custom error
            throw new CustomUserMessageAuthenticationException('Email could not be found.');
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {

    //     $user_name = $credentials['username'];
       
    //     $sql = "SELECT * FROM user WHERE username = '$user_name'";
    //    // echo $RAW_QUERY;die;
    //     $conn = $this->entityManager->getConnection();
    //     // $statement = $em->getConnection()->prepare($RAW_QUERY);
    //     // $statement->execute();
    //     // $result = $statement->fetchAll();
    //     $stmt = $conn->prepare($sql);
    //     $stmt->execute();

    //     //echo "dd==";
    //     //print_r($stmt);die;
    //     //echo "role=".$stmt['role'];die;
    //     while ($row = $stmt->fetch()) {
    //         $results[] = $row['role'] ;
    //         //print_r($results);die;
    //         $kk=$results[0];
            
    //     }
        //echo "my role id= ".$kk;die;
        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
        
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
       // print_r($providerKey);die;
       //print_r($token);die;
       //print_r($request);die;
         

       
        // if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
           
        //     return new RedirectResponse($targetPath);
            
        // }
       //echo "lll===";
       // print_r($targetPath);exit;

       $user_name = $request->request->get('_username');

       //$user_name = $credentials['username'];
       
        $sql = "SELECT * FROM user WHERE username = '$user_name'";
       // echo $RAW_QUERY;die;
        $conn = $this->entityManager->getConnection();
        // $statement = $em->getConnection()->prepare($RAW_QUERY);
        // $statement->execute();
        // $result = $statement->fetchAll();
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        //echo "dd==";
        //print_r($stmt);die;
        //echo "role=".$stmt['role'];die;
        while ($row = $stmt->fetch()) {
            $results[] = $row['role'] ;
            //print_r($results);die;
            $kk=$results[0];
            
        }
        //echo "my role id= ".$kk;die;
        
        if($kk ==  1 && $request->hasSession())//you role is 1 admin then go to rate
        {
            //echo "admin";die;
            return new RedirectResponse($this->router->generate('rate_list'));
        }
        else if($kk == 0 && $request->hasSession())//your role is user
        {
            //echo "user";die;
            return new RedirectResponse($this->router->generate('product_list'));
        }
        else
        {
            //echo "login";die;
            return new RedirectResponse($this->router->generate('/login'));
        }
        

        // For example : return new RedirectResponse($this->router->generate('some_route'));
        throw new \Exception('TODO: provide a valid redirect inside '.__FILE__);
    }

    protected function getLoginUrl()
    {
        return $this->router->generate('login');
    }
}