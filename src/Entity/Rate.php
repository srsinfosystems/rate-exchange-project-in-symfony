<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RateRepository")
 */
class Rate
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

     /**
     * @ORM\Column(type="text",length=100)
     */
    private $currency;

    /**
     * @ORM\Column(type="text")
     */
    private $rate;

     /**
     * @var \DateTime $created
     *
     *
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * 
     * @ORM\Column(type="datetime")
     */
    private $updated;




    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCurrency()
    {
        return $this->currency;
    }
    public function setCurrency($currency)
    {
         $this->currency = $currency;
    }

    public function getRate()
    {
        return $this->rate;
    }
    public function setRate($rate)
    {
        $this->rate = $rate;
    }
    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

}
