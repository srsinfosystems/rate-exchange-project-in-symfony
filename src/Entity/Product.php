<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

      /**
     * @ORM\Column(type="text",length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */

    private $inr;

     /**
     * @ORM\Column(type="text")
     */

    private $eur;

     /**
     * @ORM\Column(type="text")
     */

    private $price;



    public function getId(): ?int
    {
        return $this->id;
    }


    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        $this->name = $name;
    }


    public function getPrice()
    {
        return $this->price;
    }
    public function setPrice($price)
    {
        $this->price = $price;
    }
    
    public function getinr()
    {
        return $this->inr;
    }
    public function geteur()
    {
        return $this->eur;
    }
}
