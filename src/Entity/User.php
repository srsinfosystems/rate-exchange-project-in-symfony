<?php

namespace App\Entity;
use Symfony\Component\Security\Core\User\UserInterface;
// use FOS\UserBundle\Model\User as BaseUser;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
//class User extends BaseUser implements UserInterface
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


     /**
     * @ORM\Column(type="text",length=100)
     */
    private $username;

    /**
     * @ORM\Column(type="text")
     */
    private $password;

     /**
     * @ORM\Column(type="text")
     */
    private $role;

    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }
    public function setUsername($username)
    {
         $this->username = $username;
    }

    // public function getPassword()
    // {
    //     return $this->password;
    // }
    // public function setPassword($password)
    // {
    //      $this->password = $password;
    // }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }
    public function getRole()
    {
        return $this->role;
    }
   

    public function getSalt()
    {
        return null;
    }

    public function getRoles()
    {
        return array();//$this->role;
    }
   
    public function eraseCredentials()
    {

    }
}
