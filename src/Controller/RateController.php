<?php
namespace App\Controller;
use App\Entity\Rate;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Extension\Core\Type\TextType;
// use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;






class RateController extends Controller
{
    /**
     * @Route("/rates",name="rate_list") 
     * @Method({"GET"})
     */
    public function index()
    {
       
       $rates = $this->getDoctrine()->getRepository(Rate::class)->findAll();
       return $this->render('rates/rates-listing.html.twig',array('rates' => $rates));
    }

    /**
    * @Route("/rates/save")
    */


   

    /**
     * @Route("/rates/insert",name="insert_rate") //This is actual routing
     * @Method({"GET","POST"})
     */
    public function insert(Request $request)
    {
        $rates = new Rate();
        $form = $this->createFormBuilder($rates)
        ->add('currency',TextType::class,array(
            'attr' => array('class' => 'form-control')
        ))
        ->add('rate',TextType::class,array(
            'required' => false,
            'attr' => array('class' => 'form-control')
        ))
        ->add('save',SubmitType::class,array(
            'label' => 'Create',
            'attr' => array('class' => 'btn btn-primary mt-3')
        ))
        ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
          $article = $form->getData();
          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->persist($article);
          $entityManager->flush();
          return $this->redirectToRoute('rate_list');
        }



        return $this->render('rates/insert.html.twig',array(
            'form' => $form->createView()
        ));
        
    }
      /**
     * @Route("/rates/delete/{id}") //This is actual routing
     * @Method({"DELETE"})
     */
    public function delete(Request $request,$id)
    {
        $rates = $this->getDoctrine()->getRepository(Rate::class)->find($id);


        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($rates);
        $entityManager->flush();

        $response = new Response();
        $response->send();
    }


     /**
     * @Route("/rates/edit/{id}",name="edit_rate") //This is actual routing
     * @Method({"GET","POST"})
     */
    public function edit(Request $request,$id)
    {
        $rates = new Rate();
        $rates = $this->getDoctrine()->getRepository(Rate::class)->find($id);

        $form = $this->createFormBuilder($rates)
        ->add('currency',TextType::class,array(
            'attr' => array('class' => 'form-control')
        ))
        ->add('rate',TextType::class,array(
            'required' => false,
            'attr' => array('class' => 'form-control')
        ))
        ->add('save',SubmitType::class,array(
            'label' => 'Create',
            'attr' => array('class' => 'btn btn-primary mt-3')
        ))
        ->getForm();


        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
           

            $entityManager = $this->getDoctrine()->getManager();
           
            $entityManager->flush();
            return $this->redirectToRoute('rate_list');
        }

        return $this->render('rates/edit.html.twig',array(
            'form' => $form->createView()
        ));
        
    }


     /**
     * @Route("/rates/{id}") //This is actual routing
     * @Method({"GET"})
     */
    public function show($id)
    {
        $rates = $this->getDoctrine()->getRepository(Rate::class)->find($id);
        return $this->render('rates/show.html.twig',array('rates' => $rates));
    }
    
}