<?php
namespace App\Controller;
use App\Entity\User;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Extension\Core\Type\TextType;
// use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;



class LoginController extends Controller
{

     /**
     * @Route("/",name="login") //This is actual routing
     * @Method({"GET"})
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
         
        $errors = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();
     
        return $this->render('login/login.html.twig',array(
            'errors' => $errors,
            'username' => $lastUsername,
        ));
      
    }

   /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        $tokenStorage->setToken(null);
        $session->invalidate();
       
       
    }
   

}