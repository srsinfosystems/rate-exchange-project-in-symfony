<?php
namespace App\Controller;
use App\Entity\Product;//importing entity / model
use App\Entity\User;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Extension\Core\Type\TextType;
// use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Doctrine\ORM\EntityManagerInterface;




class ProductController extends Controller
{

    private $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
       
    }

    /**
     * @Route("/products",name="product_list") 
     * @Method({"GET"})
     * 
     */
    public function index()
    {
      
       $products = $this->getDoctrine()->getRepository(Product::class)->findAll();
       return $this->render('products/products-listing.html.twig',array('products' => $products));
    }

  

    /**
     * @Route("/product/purchase/{id}/{uid}/{price}",name="purchase_product") 
     * @Method({"POST"})
     */

    public function purchase(Request $request,$id,$uid,$price)
    {

        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();
        

        $conn1 = $this->entityManager->getConnection();

           $usd=1;

        $stmt1 = $conn1->prepare("SELECT * FROM rate where currency='INR'");
        $stmt1->execute();
        $count= $stmt1->rowCount();

      
         $stmt2 = $conn1->prepare("SELECT * FROM rate where currency='EUR'");
         $stmt2->execute();
         $count2=$stmt2->rowCount();

   
        if($count > 0 ) 
        {
         
               $posts = $stmt1->fetchAll();
                foreach($posts as $post) 
                {
                  $inr= $post['rate'];
                }
  
        }
        else if($count == 0)
        {
             $url = 'https://api.exchangeratesapi.io/latest?base=USD';
              $ch = curl_init($url);
              curl_setopt($ch, CURLOPT_TIMEOUT, 5);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              $data = curl_exec($ch);
              curl_close($ch);
             
            $data1= json_decode($data, true);
              
             $inr = $data1['rates']['INR'];

        }

        if($count2 > 0) 
          {
            
                 $posts2 = $stmt2->fetchAll();
                  foreach($posts2 as $post) 
                  {
                   
                    $eur= $post['rate'];
                   
                  }
               
          }

       
       else if($count2 == 0)//takle data from api
        { 
              $url = 'https://api.exchangeratesapi.io/latest?base=USD';
              $ch = curl_init($url);
              curl_setopt($ch, CURLOPT_TIMEOUT, 5);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              $data = curl_exec($ch);
              curl_close($ch);
            

            $data1= json_decode($data, true);
              
           
             $eur = $data1['rates']['EUR'];
         
             

        }
    

             $my_usd = $price * $usd;

             $my_inr = $price * $inr;
            
             $my_eur = $price * $eur;


         


        $sql ="INSERT INTO expense(uid,pid,rate,inr,eur) VALUES ('$uid','$id','$my_usd','$my_inr','$my_eur')";
    
         $conn = $this->entityManager->getConnection();
        
         $stmt = $conn->prepare($sql);
       
         if($stmt->execute())
         {
         
          
             return $this->render('products/products-listing.html.twig',array('products' => $products));
         }
   
    }
    
    
}