const rates = document.getElementById('rates_table');

if(rates)
{
    rates.addEventListener('click',e =>
    {
        if(e.target.className === 'btn btn-danger delete-rate')
        {
            if(confirm('Are you sure?'))
            {
                const id = e.target.getAttribute('data-id');

              

               fetch(`/rate/delete/${id}`,{
                method:'DELETE',
               }).then(res => window.location.reload());
            }
        }
        
    });
}

const rates1 = document.getElementById('products_table');

if(rates1)
{
    rates1.addEventListener('click',e =>
    {
        if(e.target.className === 'btn btn-danger buy-product')
        {
            if(confirm('Are you sure?You want to purchase?'))
            {
                const id = e.target.getAttribute('data-id');
                const uid = e.target.getAttribute('data-id1');
                const price = e.target.getAttribute('data-id2');

               post(`/product/purchase/${id}/${uid}/${price}`,{
                method:'POST',
               }).then(res => window.location.reload());
            }
        }
        
    });
}
